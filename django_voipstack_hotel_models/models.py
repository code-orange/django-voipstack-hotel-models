from django.conf import settings

from django_mdat_customer.django_mdat_customer.models import *


class HotelCustomerUsers(models.Model):
    id = models.BigAutoField(primary_key=True)
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.DO_NOTHING)

    class Meta:
        db_table = "hotel_customer_users"
